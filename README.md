# Information / Информация

SPEC-файл для создания RPM-пакета **meta-xenforo**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/meta`.
2. Установить пакет: `dnf install meta-xenforo`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)