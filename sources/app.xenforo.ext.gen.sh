#!/usr/bin/env bash

(( ${EUID} == 0 )) &&
  { echo >&2 "This script should not be run as root!"; exit 1; }

# -------------------------------------------------------------------------------------------------------------------- #
# Get options.
# -------------------------------------------------------------------------------------------------------------------- #

OPTIND=1

while getopts "n:h" opt; do
  case ${opt} in
    n)
      ext_name="${OPTARG}"
      ;;
    h|*)
      echo "-n [ext_name]"
      exit 2
      ;;
    \?)
      echo "Invalid option: -${OPTARG}."
      exit 1
      ;;
    :)
      echo "Option -${OPTARG} requires an argument."
      exit 1
      ;;
  esac
done

shift $(( ${OPTIND} - 1 ))

[[ -z "${ext_name}" ]] && exit 1

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

ext_id="ext_$(date +%s%N | sha512sum | fold -w 8 | head -n 1)"
d_data="${ext_id}/_data"

mkdir -p ${d_data}

cat > "${d_data}/options.xml" <<EOF
<?xml version="1.0" encoding="utf-8"?>
<options>
  <option option_id="${ext_id}_NAME" edit_format="textbox" data_type="string">
    <default_value><![CDATA[]]></default_value>
    <relation group_id="${ext_id}" display_order="10"/>
  </option>
</options>
EOF

cat > "${d_data}/option_groups.xml" <<EOF
<?xml version="1.0" encoding="utf-8"?>
<option_groups>
  <group group_id="${ext_id}" icon="fa-cubes" display_order="9999" debug_only="0"/>
</option_groups>
EOF

cat > "${d_data}/phrases.xml" <<EOF
<?xml version="1.0" encoding="utf-8"?>
<phrases>
  <phrase title="${ext_id}_NAME" version_id="1000070" version_string="1.0.0"><![CDATA[]]></phrase>
  <phrase title="option.${ext_id}_NAME" version_id="1000070" version_string="1.0.0"><![CDATA[]]></phrase>
  <phrase title="option_explain.${ext_id}_NAME" version_id="1000070" version_string="1.0.0"><![CDATA[]]></phrase>
  <phrase title="option_group.${ext_id}" version_id="1000070" version_string="1.0.0"><![CDATA[]]></phrase>
  <phrase title="option_group_description.${ext_id}" version_id="1000070" version_string="1.0.0"><![CDATA[]]></phrase>
</phrases>
EOF

cat > "${d_data}/templates.xml" <<EOF
<?xml version="1.0" encoding="utf-8"?>
<templates>
  <template type="public"
            title="${ext_id}_NAME"
            version_id="1000070"
            version_string="1.0.0"><![CDATA[]]></template>
</templates>
EOF

cat > "${d_data}/template_modifications.xml" <<EOF
<?xml version="1.0" encoding="utf-8"?>
<template_modifications>
  <modification type="public"
                template="PAGE_CONTAINER"
                modification_key="${ext_id}_NAME"
                description=""
                execution_order="9999"
                enabled="1"
                action="str_replace">
    <find><![CDATA[]]></find>
    <replace><![CDATA[]]></replace>
  </modification>
</template_modifications>
EOF

cat > "${ext_id}/addon.json" <<EOF
{
  "legacy_addon_id": "",
  "title": "${ext_id}",
  "description": "(${ext_id})",
  "version_id": 1000070,
  "version_string": "1.0.0",
  "dev": "Extensions Development",
  "dev_url": "https://exts.dev/",
  "faq_url": "https://exts.dev/#xenforo",
  "support_url": "https://exts.dev/#xenforo",
  "extra_urls": {
    "Source": "https://gitlab.com/marketplace-xenforo/xenforo-ext-${ext_name}",
    "Issues": "https://gitlab.com/marketplace-xenforo/xenforo-ext-${ext_name}/issues"
  },
  "require": {
    "XF": [
      2010070,
      "XenForo 2.1.0+"
    ],
    "php": [
      "7.3.0",
      "PHP 7.3.0+"
    ]
  },
  "icon": "fa-cubes"
}
EOF

cat > "${ext_id}/composer.json" <<EOF
{
  "name": "marketplace/xenforo-ext-${ext_name}",
  "type": "xenforo-extension",
  "description": "",
  "license": "GPL-3.0-or-later",
  "homepage": "https://exts.dev/#xenforo",
  "keywords": [
    "xenforo"
  ],
  "authors": [
    {
      "name": "x06174",
      "homepage": "https://x06174.gitlab.io/",
      "role": "Developer"
    }
  ],
  "support": {
    "issues": "https://gitlab.com/marketplace-xenforo/xenforo-ext-${ext_name}/issues",
    "source": "https://gitlab.com/marketplace-xenforo/xenforo-ext-${ext_name}"
  },
  "require": {
    "composer/installers": "~1.0"
  },
  "autoload": {
    "psr-4": {}
  },
  "config": {
    "optimize-autoloader": true,
    "prepend-autoloader": false
  },
  "extra": {
    "installer-name": "${ext_id}"
  }
}
EOF

cat > "${ext_id}/Setup.php" <<EOF
<?php

namespace MARKETPLACE\\${ext_id};

use XF\AddOn\AbstractSetup;

class Setup extends AbstractSetup {
  public function install( array \$stepParams = [] ) {
    // TODO: Implement install() method.
  }

  public function upgrade( array \$stepParams = [] ) {
    // TODO: Implement upgrade() method.
  }

  public function uninstall( array \$stepParams = [] ) {
    // TODO: Implement uninstall() method.
  }
}
EOF

cat > "${ext_id}/README.md" <<EOF
# Information / Информация

| Property      | Value                             |
| ------------- | --------------------------------- |
| ID            | \`${ext_id}\`                     |
| Type          | Add-on                            |
| Version       | 1.0.0                             |
| License       | GPL-3.0                           |
| Language      | Russian                           |
| Requirements  | XenForo 2.1                       |
| Authors       | [x06174](mailto:x06174@gmail.com) |

{DESC}

## Install / Установка

1. [Загрузить](https://gitlab.com/marketplace-xenforo/xenforo-ext-${ext_name}/-/tags) архив с последней версией расширения.
2. Распаковать содержимое архива в \`/src/addons/MARKETPLACE/${ext_id}/\`, сохраняя структуру директорий.
3. Зайти в **AdminCP**, далее *Add-ons*, и установить необходимое расширение.

## Update / Обновление

1. [Загрузить](https://gitlab.com/marketplace-xenforo/xenforo-ext-${ext_name}/-/tags) архив с новой версией расширения.
2. Распаковать содержимое архива в \`/src/addons/MARKETPLACE/${ext_id}/\`, сохраняя структуру директорий, заменяя существующие файлы и папки.
3. Зайти в **AdminCP**, далее *Add-ons*, и обновить необходимое расширение.

## Donations / Пожертвования

- [Donating](https://donating.gitlab.io/)

EOF