#!/usr/bin/env bash

(( ${EUID} == 0 )) &&
  { echo >&2 "This script should not be run as root!"; exit 1; }

# -------------------------------------------------------------------------------------------------------------------- #
# Get options.
# -------------------------------------------------------------------------------------------------------------------- #

OPTIND=1

while getopts "d:u:h" opt; do
  case ${opt} in
    d)
      db_name="${OPTARG}"
      ;;
    u)
      db_user="${OPTARG}"
      ;;
    h|*)
      echo "-d [db_name] -u [db_user]"
      exit 2
      ;;
    \?)
      echo "Invalid option: -${OPTARG}."
      exit 1
      ;;
    :)
      echo "Option -${OPTARG} requires an argument."
      exit 1
      ;;
  esac
done

shift $(( ${OPTIND} - 1 ))

[[ -z "${db_name}" ]] || [[ -z "${db_user}" ]] && exit 1

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

# Set timestamp (as count).
timestamp=$( date -u '+%Y-%m-%d.%H-%M-%S' )

# Set sleep time.
sleep="5"

# Get apps.
dump=$( which mysqldump )
php=$( which php )

# Set CMS.
cms_type="XenForo"
cms_user="root"
cms_pass=$( head /dev/urandom | tr -dc A-Za-z0-9 | head -c 16 )
cms_mail="${cms_user}@localhost.local"
cms_path="$( basename $( pwd )/public_html )"

# -------------------------------------------------------------------------------------------------------------------- #
# Install.
# -------------------------------------------------------------------------------------------------------------------- #

[[ ! -f "${cms_path}/cmd.php" ]] && { echo >&2 "File cmd.php DOES NOT exists!"; exit 1; }

# CMS: Core.
echo "--- Install ${cms_type}: Core | User: ${cms_user} | Password: ${cms_pass}"
${php} "${cms_path}/cmd.php" xf:install \
--user="${cms_user}"                    \
--password="${cms_pass}"                \
--email="${cms_mail}"                   \
--skip-statistics -n
echo "" && sleep ${sleep}

# CMS: Add-on.
for i in XFES XFMG XFRM; do
  echo "--- Install ${cms_type}: Add-on ${i}"
  ${php} "${cms_path}/cmd.php" xf:addon-install ${i} -n
  echo "" && sleep ${sleep}
done

# -------------------------------------------------------------------------------------------------------------------- #
# Backup.
# -------------------------------------------------------------------------------------------------------------------- #

# Check "public_html" directory.
[[ ! -d "${cms_path}" ]] && { echo >&2 "Directory public_html DOES NOT exists!"; exit 1; }

# Backup database.
echo "--- Backup ${cms_type}: Database ${db_name} | User: ${db_user}"
${dump} -h "127.0.0.1" -u "${db_user}" -p --opt ${db_name} > "${db_name}.${timestamp}.sql"  \
  && tar -cJf "${db_name}.${timestamp}.sql.tar.xz" "${db_name}.${timestamp}.sql"            \
  && rm -f "${db_name}.${timestamp}.sql"
echo "" && sleep ${sleep}

# Backup files.
echo "--- Backup ${cms_type}: Files"
tar -cJf "${db_name}.${timestamp}.tar.xz" "${cms_path}"
echo "" && sleep ${sleep}

# Create backup directory.
mkdir "${timestamp}" && mv *.tar.xz "${timestamp}"

# -------------------------------------------------------------------------------------------------------------------- #
# Exit.
# -------------------------------------------------------------------------------------------------------------------- #

exit 0
