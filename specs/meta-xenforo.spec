%global app                     xenforo
%global d_bin                   %{_bindir}

Name:                           meta-xenforo
Version:                        1.0.1
Release:                        1%{?dist}
Summary:                        META-package for install and upgrade XenForo
License:                        GPLv3

Source10:                       app.%{app}.backup.sh
Source11:                       app.%{app}.install.sh
Source12:                       app.%{app}.upgrade.sh
Source13:                       app.%{app}.ext.gen.sh

Requires:                       meta-system

%description
META-package for install and upgrade XenForo.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0755 %{SOURCE10} \
  %{buildroot}%{d_bin}/app.%{app}.backup.sh
%{__install} -Dp -m 0755 %{SOURCE11} \
  %{buildroot}%{d_bin}/app.%{app}.install.sh
%{__install} -Dp -m 0755 %{SOURCE12} \
  %{buildroot}%{d_bin}/app.%{app}.upgrade.sh
%{__install} -Dp -m 0755 %{SOURCE13} \
  %{buildroot}%{d_bin}/app.%{app}.ext.gen.sh


%files
%{d_bin}/app.%{app}.backup.sh
%{d_bin}/app.%{app}.install.sh
%{d_bin}/app.%{app}.upgrade.sh
%{d_bin}/app.%{app}.ext.gen.sh


%changelog
* Sun Oct 13 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-1
- NEW: XenForo extension generator script.

* Thu Aug 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-9
- UPD: Shell scripts.

* Thu Aug 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-8
- UPD: Shell scripts.

* Thu Aug 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-7
- UPD: Shell scripts.

* Wed Jul 31 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-6
- UPD: SPEC-file.

* Mon Jul 29 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-5
- UPD: Shell scripts.

* Thu Jul 11 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-4
- UPD: Shell scripts.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-3
- UPD: Shell scripts.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-2
- UPD: Shell scripts.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-1
- Initial build.
